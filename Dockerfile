FROM python:3.8.5-slim

LABEL Name="Python Flask App" 

ARG srcDir=src
WORKDIR /app
RUN mkdir -p /ssl
COPY $srcDir/requirements.txt .
RUN pip install -r requirements.txt

COPY $srcDir .
COPY cert.crt /ssl/mydomain.crt
COPY private.key /ssl/mydomain.key


EXPOSE 5000

CMD python main.py
